package list.utils.constants;

public abstract class ExceptionsMessages {
	
	public static final String LIST_SIZE_MUST_BE_GREATER_THAN_ZERO 		= "Given size should be greater than zero." ;
	public static final String GIVEN_LIST_IS_NULL					 	= "Given List is NULL." ;
}
