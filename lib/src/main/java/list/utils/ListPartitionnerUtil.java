/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package list.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import list.utils.constants.ExceptionsMessages;
import list.utils.exceptions.BadSizeExeption;
import list.utils.exceptions.NullGivenListException;

public class ListPartitionnerUtil {
  
	/**
	 * 
	 * @param <T> Generic type for flexible use
	 * @param list : list to partition
	 * @param size : size of each part
	 * @return list of size-elements sized partitions
	 * 
	 * <br><br>Throws {@link NullGivenListException} if given list is NULL
	 * <br>Throws {@link BadSizeExeption} if given partition size is negative or equal to Zero. 
	 * 
	 * <p>
	 * Examples: <br>
	 * 		<ul>
	 * 			<li> partition([1, 2, 3, 4, 5, 6, 7, 8], 3) = [[1, 2, 3], [3, 5, 6], [7, 8] ] ;</li>
	 * 			<li> partition([1, 2, 3, 4], 5) = [ [1, 2, 3, 4, 5] ] ;</li>
	 * 			<li> partition([] , N) = [] ;</li>
	 * 		<ul>
	 * </p
	 */
    public static <T> List<List<T>> partition(List<T> list, final int size){
    
    	List<List<T>> res = new ArrayList<>();
    	
    	if(list == null)
    		throw new NullGivenListException(ExceptionsMessages.GIVEN_LIST_IS_NULL) ;
    	
    	if(list.isEmpty())
    		return res;
    	
    	
    	if(size <= 0)
    		throw new BadSizeExeption(ExceptionsMessages.LIST_SIZE_MUST_BE_GREATER_THAN_ZERO) ;
    
    	if(size >= list.size()) {
    		res.add(list) ;
    		return res ;
    	}
    	
    	return new ArrayList<>(IntStream.range(0, list.size())
    			.boxed()
    			.collect(Collectors.groupingBy(e->e/size,Collectors.mapping(e->list.get(e), Collectors.toList())))
    			.values());
    }
    
}
