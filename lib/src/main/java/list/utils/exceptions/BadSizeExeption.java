package list.utils.exceptions;

public class BadSizeExeption extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7632371839722094805L;
	
	public BadSizeExeption(String message) {
		super(message) ;
	}
}
