package list.utils.exceptions;

public class NullGivenListException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2521506389273522193L;
	
	public NullGivenListException(String message) {
		super(message) ;
	}

}
