package list.utils;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import list.utils.exceptions.BadSizeExeption;
import list.utils.exceptions.NullGivenListException;

public class ListPartitionnerUtilTest {
    
	static final List<Integer> integers = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
    
    @Test(expected = BadSizeExeption.class) 
    public void testPartition_shouldThrowBadSizeExeption() {
    	ListPartitionnerUtil.partition(integers, 0) ;
    }
    
    @Test(expected = NullGivenListException.class)
    public void testPartition_givenListIsNull_shouldThrowException() {
    	ListPartitionnerUtil.partition(null, 1) ;
    }

    @Test 
    public void testPartition_givenPartitionSizeGreaterThanListSize() {
    	List<List<Integer>> partitions =  ListPartitionnerUtil.partition(integers, integers.size() + 1) ;
    	assertTrue(partitions.size() == 1 && partitions.get(0).equals(integers)) ;
    }
    
    @Test
    public void testPartition_emptyGivenList() {
    	List<List<Integer>> partitions = ListPartitionnerUtil.partition(new ArrayList<>(), 5) ;
    	assertTrue(partitions.isEmpty());
    }
    
    @Test
    public void testPartition_shouldReturnGivenListSizeSubLists() {
    	List<List<Integer>> partitions = ListPartitionnerUtil.partition(integers, 1) ;
    	System.out.println(partitions);
    	assertTrue(partitions.size() == integers.size());
    }
    
    @Test
    public void testPartition_lastPartitionShouldBeLteGivenSize() {
    	int pSize = 6 ;
    	List<List<Integer>> partitions = ListPartitionnerUtil.partition(integers, pSize) ;
    	System.out.println(partitions);
    	assertTrue(partitions.size() == (integers.size() / pSize) + (integers.size()%pSize == 0 ? 0 : 1));
    }
    
}
